package com.galvanize;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;

import static java.lang.String.format;
import static java.util.stream.Collectors.joining;

public class SQLBuilder {

    private final DatabaseDriver driver;
    private ArrayList<String> columns = new ArrayList<>();
    private ArrayList<String> tables = new ArrayList<>();
    private LinkedHashMap<String, String> whereClauses = new LinkedHashMap<>();

    public SQLBuilder(DatabaseDriver driver) {
        this.driver = driver;
    }

    public SQLBuilder select(String... columns) {
        this.columns.addAll(Arrays.asList(columns));
        return this;
    }

    public SQLBuilder from(String... tables) {
        this.tables.addAll(Arrays.asList(tables));
        return this;
    }

    public SQLBuilder where(String column, String value) {
        this.whereClauses.put(column, value);
        return this;
    }

    public String build() {
        return "SELECT " +
                columns.stream()
                        .map(this.driver::quoteColumn)
                        .collect(joining(", ")) +
                "\nFROM " +
                tables.stream()
                        .map(this.driver::quoteTable)
                        .collect(joining(", ")) +
                "\nWHERE " +
                whereClauses.entrySet().stream()
                        .map(entry -> whereClause(entry.getKey(), entry.getValue()))
                        .collect(joining(" AND "));
    }

    private String whereClause(String column, String value) {
        return format("%s = %s", this.driver.quoteColumn(column), this.driver.quoteValue(value));
    }
}