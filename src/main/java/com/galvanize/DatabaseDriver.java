package com.galvanize;

public interface DatabaseDriver {
    String quoteColumn(String column);
    String quoteTable(String table);
    String quoteValue(String value);
}
